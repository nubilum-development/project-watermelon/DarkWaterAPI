package kiinse.plugins.api.darkwaterapi.loader;

import kiinse.plugins.api.darkwaterapi.DarkWaterAPI;
import kiinse.plugins.api.darkwaterapi.files.config.Configuration;
import kiinse.plugins.api.darkwaterapi.files.config.utils.Config;
import kiinse.plugins.api.darkwaterapi.files.filemanager.YamlFile;
import kiinse.plugins.api.darkwaterapi.files.filemanager.interfaces.FilesKeys;
import kiinse.plugins.api.darkwaterapi.files.filemanager.utils.File;
import kiinse.plugins.api.darkwaterapi.files.messages.DarkWaterMessages;
import kiinse.plugins.api.darkwaterapi.files.messages.interfaces.Messages;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.logging.Level;

/**
 * Класс плагина DarkWaterJavaPlugin
 */
public abstract class DarkWaterJavaPlugin extends JavaPlugin {

    /**
     * Файл конфига
     */
    protected static final FilesKeys configurationFileName = File.CONFIG_YML;

    /**
     * Конфигурация
     */
    protected YamlFile configuration;

    /**
     * Сообщения
     */
    protected Messages messages;

    @Override
    public void onEnable() {
        start();
    }

    @Override
    public void onDisable() {
        stop();
    }

    protected void start() {
        try {
            getLogger().setLevel(Level.CONFIG);
            sendLog("Loading " + getName() + "...");
            configuration = new Configuration(this);
            messages = new DarkWaterMessages(this);
            onStart();
            getDarkWaterAPI().getPluginManager().registerPlugin(this);
            sendInfo();
        } catch (Exception e) {
            sendLog(Level.SEVERE, "Error on loading " + getName() + "! Message: " + e.getMessage());
        }
    }

    protected void stop() {
        try {
            sendLog("Disabling " + getName() + "...");
            onStop();
            sendConsole(" &6|==============================");
            sendConsole(" &6|  &f" + getName() + " &cdisabled!");
            sendConsole(" &6|==============================");
        } catch (Exception e) {
            sendLog(Level.SEVERE, "Error on disabling " + getName() + "! Message: " + e.getMessage());
        }
    }

    /**
     * Действия при запуске плагина
     */
    public abstract void onStart() throws Exception;

    /**
     * Действия при выключении плагина
     */
    public abstract void onStop() throws Exception;

    /**
     * Получение имени файла конфигурации
     */
    public FilesKeys getConfigurationFileName() {
        return configurationFileName;
    }

    /**
     * Получение сообщений плагина
     * @return Messages плагина {@link Messages}
     */
    public Messages getMessages() {
        return messages;
    }

    /**
     * Получение конфигурации плагина
     * @return Конфиг плагина {@link YamlFile}
     */
    public YamlFile getConfiguration() {
        return configuration;
    }

    /**
     * Получение instance DarkWaterAPI
     * @return {@link DarkWaterAPI}
     */
    public DarkWaterAPI getDarkWaterAPI() {return DarkWaterAPI.getInstance();}

    /**
     * Перезапуск плагина (Выполняет действия при выключении, а затем при включении)
     */
    public void restart() {
        try {
            sendLog("Reloading " + getName() + "...");
            onStop();
            configuration = new Configuration(this);
            messages = new DarkWaterMessages(this);
            onStart();
            sendConsole(" &6|==============================");
            sendConsole(" &6|  &f" + getName() + " &areloaded!");
            sendConsole(" &6|==============================");
        } catch (Exception e) {
            sendLog(Level.SEVERE, "Error on reloading " + getName() + "! Message: " + e.getMessage());
        }
    }

    /**
     * Получение данных с плагина
     * По умолчанию возвращает версию плагина, версию API, авторов и описание
     * @return JSONObject с данными
     */
    public JSONObject getPluginData() {
        var description = this.getDescription();
        var map = new HashMap<String, String>();
        map.put("authors", String.valueOf(description.getAuthors()));
        map.put("version", description.getVersion());
        map.put("api", description.getAPIVersion());
        map.put("description", description.getDescription());
        return new JSONObject(map);
    }

    /**
     * Отправка логов в консоль уровня INFO
     * @param msg Сообщение
     */
    public void sendLog(String msg) {
        sendLog(Level.INFO, msg);
    }

    /**
     * Отправка логов в консоль
     * @param level Уровень логирования
     * @param msg Сообщение
     */
    public void sendLog(Level level, String msg) {
        if (level == Level.INFO) {
            sendConsole("&6[&b" + getName() + "&6]&a " + msg);
            return;
        }
        if (level == Level.WARNING) {
            sendConsole("&6[&b" + getName() + "&f/&cWARN&6] " + msg);
            return;
        }
        if (level == Level.CONFIG && getDarkWaterAPI().getConfiguration() != null && getDarkWaterAPI().getConfiguration().getBoolean(Config.DEBUG)) {
            sendConsole("&6[&b" + getName() + "&f/&dDEBUG&6] " + msg);
            return;
        }
        getLogger().log(level, msg);
    }

    /**
     * Отправка в консоль сообщения
     * @param message Сообщение
     */
    public void sendConsole(String message) {
        Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    /**
     * Отправка информации от плагина
     */
    protected void sendInfo() {
        sendConsole(" &6|==============================");
        sendConsole(" &6|  &f" + getName() + " &bloaded!");
        sendConsole(" &6|  &bAuthors: &f" + getDescription().getAuthors());
        sendConsole(" &6|  &bWebsite: &f" + getDescription().getWebsite());
        sendConsole(" &6|  &bPlugin version: &f" + getDescription().getVersion());
        sendConsole(" &6|==============================");
    }
}
