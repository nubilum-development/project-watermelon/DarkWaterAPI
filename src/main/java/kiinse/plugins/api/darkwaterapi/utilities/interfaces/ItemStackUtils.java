package kiinse.plugins.api.darkwaterapi.utilities.interfaces;

import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.potion.PotionType;

import java.util.List;
import java.util.UUID;

/**
 * Класс утилит для работы с ItemStack {@link ItemStack}
 */
@SuppressWarnings("unused")
public interface ItemStackUtils {

    /**
     * Получение ItemStack
     * @param material Материал
     * @param name Имя
     * @param lore Описание
     * @param amount Количество
     * @return ItemStack
     */
    ItemStack getItemStack(Material material, String name, List<Component> lore, int amount);

    /**
     * Получение ItemStack зелья
     * @param name Имя
     * @param lore Описание
     * @param type Тип зелья
     * @param amount Количество
     * @return ItemStack
     */
    ItemStack getPotionItemStack(String name, List<Component> lore, PotionType type, int amount);

    /**
     * Получения рецепта в печке
     * @param key ключ
     * @param result ItemStack результат
     * @param experience Количество опыта
     * @param cookingTime Время готовки
     * @return FurnaceRecipe
     */
    FurnaceRecipe getFurnaceRecipe(String key, ItemStack result, float experience, int cookingTime);

    /**
     * Получение рецепта без определённого размещения в сетке
     * @param key ключ
     * @param result ItemStack результат
     * @param ingredient1 ItemStack ингредиент первый
     * @param ingredient2 ItemStack ингредиент второй
     * @return ShapelessRecipe
     */
    ShapelessRecipe getShapelessRecipe(String key, ItemStack result, ItemStack ingredient1, ItemStack ingredient2);

    /**
     * Получение рецепта без определённого размещения в сетке
     * @param key ключ
     * @param result ItemStack результат
     * @param ingredient ItemStack ингредиент
     * @return ShapelessRecipe
     */
    ShapelessRecipe getShapelessRecipe(String key, ItemStack result, ItemStack ingredient);

    ShapedRecipe getShapedRecipe(String key, ItemStack result);

    /**
     * Получение головы игрока
     * @param player Игрок
     * @param displayName Имя
     * @param lore Описание
     * @return ItemStack
     */
    ItemStack getPlayerHead(Player player, String displayName, List<Component> lore);

    /**
     * Получение головы игрока
     * @param player UUID игрока
     * @param displayName Имя
     * @param lore Описание
     * @return ItemStack
     */
    ItemStack getPlayerHead(UUID player, String displayName, List<Component> lore);

    /**
     * Сравнение ItemStack с материалом
     * @return true если одинаковы
     */
    boolean checkItemStack(ItemStack stack, Material material);
}
