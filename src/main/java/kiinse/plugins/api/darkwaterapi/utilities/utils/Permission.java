package kiinse.plugins.api.darkwaterapi.utilities.utils;

import kiinse.plugins.api.darkwaterapi.utilities.interfaces.PermissionsKeys;

public enum Permission implements PermissionsKeys {
    LOCALE_GET,
    LOCALE_CHANGE,
    LOCALE_LIST,
    LOCALE_HELP,
    DARKWATER_RELOAD,
    DARKWATER_ENABLE,
    DARKWATER_DISABLE,
    DARKWATER_STATISTIC
}
