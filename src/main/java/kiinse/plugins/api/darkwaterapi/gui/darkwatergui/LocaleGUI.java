package kiinse.plugins.api.darkwaterapi.gui.darkwatergui;

import kiinse.plugins.api.darkwaterapi.DarkWaterAPI;
import kiinse.plugins.api.darkwaterapi.files.locale.interfaces.Locale;
import kiinse.plugins.api.darkwaterapi.files.locale.interfaces.LocaleStorage;
import kiinse.plugins.api.darkwaterapi.files.messages.utils.Message;
import kiinse.plugins.api.darkwaterapi.gui.GUI;
import kiinse.plugins.api.darkwaterapi.gui.darkwatergui.items.*;
import kiinse.plugins.api.darkwaterapi.utilities.PlayerUtils;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocaleGUI extends GUI {
    private final LocaleStorage localeStorage = DarkWaterAPI.getInstance().getLocaleStorage();

    public LocaleGUI(int page, String name, Locale playerLocale) {
        super(36, name);
        int position = 9;
        var plugin = DarkWaterAPI.getInstance();
        var locales = getGuiPages();
        var messages = plugin.getMessages();
        for (var locale : locales.get(page)) {
            var item = new ItemStack(Material.GOLD_BLOCK);
            var list = new ArrayList<Component>();
            var meta = item.getItemMeta();
            list.add(messages.getComponentMessage(playerLocale, Message.SET_THIS_LOCALE));
            meta.lore(list);
            item.setItemMeta(meta);
            setItem(new LocaleItem(position, "&f" + locale.toString(), item, player -> {
                player.performCommand("locale set " + locale);
                delete();
            }));
            position++;
        }
        if (locales.containsKey(page-1)) {
            setItem(new PreviousPageItem(plugin, playerLocale, (player -> {
                delete();
                PlayerUtils.playSound(player, Sound.BLOCK_AMETHYST_BLOCK_STEP);
                new LocaleGUI(page-1, name, playerLocale).open(player);
            })));
        }
        if (locales.size() > 1) {
            setItem(new CurrentPageItem(plugin, playerLocale, page, (player -> {})));
        }
        if (locales.containsKey(page+1)) {
            setItem(new NextPageItem(plugin, playerLocale, (player -> {
                delete();
                PlayerUtils.playSound(player, Sound.BLOCK_AMETHYST_BLOCK_STEP);
                new LocaleGUI(page+1, name, playerLocale).open(player);
            })));
        }
        setItem(new ExitItem(plugin, playerLocale, (player -> {
            delete();
            PlayerUtils.playSound(player, Sound.BLOCK_AMETHYST_BLOCK_STEP);
        })));
    }

    public Map<Integer, List<Locale>> getGuiPages() {
        var hashmap = new HashMap<Integer, List<Locale>>();
        var list = new ArrayList<Locale>();
        int localesCount = 0;
        int size = 0;
        int page = 1;
        for (var locale : localeStorage.getAllowedLocalesList()) {
            size++;
            list.add(locale);
            if (size == 9 || localesCount == localeStorage.getAllowedLocalesList().size()-1) {
                hashmap.put(page, new ArrayList<>(list));
                page++;
                size = 0;
                list.clear();
            }
            localesCount++;
        }
        return hashmap;
    }
}
