package kiinse.plugins.api.darkwaterapi.databases.interfaces;

import kiinse.plugins.api.darkwaterapi.databases.SQLConnectionSettings;
import kiinse.plugins.api.darkwaterapi.files.config.utils.Config;
import kiinse.plugins.api.darkwaterapi.loader.DarkWaterJavaPlugin;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

@SuppressWarnings("unused")
public abstract class Postgresql {

    private Connection connection;

    private DSLContext context;

    private final DarkWaterJavaPlugin plugin;

    /**
     * Подключение к БД
     * @param plugin Главный класс плагина, наследуемый от DarkWaterJavaPlugin;
     * @throws Exception Ошибка если база данных уже подключена или возникающая ошибка в методе "registerConnection"
     */
    protected Postgresql(DarkWaterJavaPlugin plugin) throws Exception {
        connect();
        this.plugin = plugin;
    }

    /**
     * Подключение к БД
     * @throws Exception Ошибка если база данных уже подключена или возникающая ошибка в методе "registerConnection"
     */
    public void connect() throws Exception {
        if (!isConnected()) {
            plugin.sendLog("Connecting to database...");
            System.getProperties().setProperty("org.jooq.no-logo", "true");
            System.getProperties().setProperty("org.jooq.no-tips", "true");
            connection = registerConnection(getSettings(plugin));
            context = DSL.using(connection, SQLDialect.POSTGRES);
            createDataBases(context);
            plugin.sendLog("Database connected.");
        } else {
            throw new SQLException("Database already connected!");
        }
    }

    /**
     * Получение настроек
     * @param plugin Главный класс плагина, наследуемый от DarkWaterJavaPlugin;
     * @return Данные подключения к базе данных. По умолчанию берёт pg.host, pg.port, pg.dbname, pg.login, pg.password из конфига плагина.
     */
    public SQLConnectionSettings getSettings(DarkWaterJavaPlugin plugin) {
        var settings = new SQLConnectionSettings();
        var config = plugin.getConfiguration();
        settings.setHost(config.getString(Config.PG_HOST));
        settings.setPort(config.getString(Config.PG_PORT));
        settings.setDbName(config.getString(Config.PG_DBNAME));
        settings.setLogin(config.getString(Config.PG_LOGIN));
        settings.setPassword(config.getString(Config.PG_PASSWORD));
        settings.setURLDriver("postgresql");
        return settings;
    }

    /**
     * Создание подключения к базе данных
     * @param settings Данные для подключения
     * @return Подключение
     * @throws Exception При различных ошибках покдлчюения
     */
    public abstract Connection registerConnection(SQLConnectionSettings settings) throws Exception;

    /**
     * Создание таблиц в базе данных
     * @param context Контекст подключения
     */
    public abstract void createDataBases(DSLContext context);

    /**
     * Получение DSLContext
     * @return DSLContext
     */
    public DSLContext getContext() {
        return this.context;
    }

    /**
     * Получение подключения к БД
     * @return Connection
     */
    public Connection getConnection() {return this.connection;}

    /**
     * Получение строки для подключения к БД
     * @param settings Данные для подключения
     * @return "jdbc:" + settings.getUrlDriver() + "://" + settings.getHost() + ":" + settings.getPort() + "/"
     */
    public String getURL(SQLConnectionSettings settings) {
        var url = "jdbc:" + settings.getUrlDriver() + "://" + settings.getHost() + ":" + settings.getPort() + "/";
        plugin.sendLog(Level.CONFIG, "Database connection url: &d" + url);
        return url;
    }

    /**
     * Получение настроек для подключения
     * @param settings Данные для подключения
     * @return Настройки, содержащие в себе логин, пароль и кодировку UTF-8
     */
    public Properties getProperties(SQLConnectionSettings settings) {
        var connInfo = new Properties();
        connInfo.setProperty("user", settings.getLogin());
        connInfo.setProperty("password", settings.getPassword());
        connInfo.setProperty("useUnicode", "true");
        connInfo.setProperty("characterEncoding", "UTF-8");
        plugin.sendLog(Level.CONFIG, "Database user: &d" + settings.getLogin());
        plugin.sendLog(Level.CONFIG, "Database password: &d****" + settings.getLogin().substring(4));
        return connInfo;
    }

    /**
     * @return false если Connection == null
     */
    public boolean isConnected() {
        return (connection != null);
    }

}
