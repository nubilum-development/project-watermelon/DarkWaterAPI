package kiinse.plugins.api.darkwaterapi.schedulers.interfaces;

import kiinse.plugins.api.darkwaterapi.loader.DarkWaterJavaPlugin;

/**
 * Класс scheduler
 */
public abstract class Scheduler {

    private final String name;
    private boolean started;
    public final DarkWaterJavaPlugin plugin;

    /**
     * Инициализация плагина и имени
     * @param plugin Плагин
     * @param name Имя присвоенное Scheduler
     */
    protected Scheduler(DarkWaterJavaPlugin plugin, String name) {
        this.plugin = plugin;
        this.name = name;
    }

    /**
     * Запустить scheduler
     */
    public void start() {
        onStartup();
        started = true;
        var nameSub = name.split("\\.");
        plugin.sendLog("Scheduler '&b" + nameSub[nameSub.length-1] + "&a' started!");
    }

    /**
     * Остановить scheduler
     */
    public void stop() {
        onStopping();
        started = false;
        var nameSub = name.split("\\.");
        plugin.sendLog("Scheduler '&b" + nameSub[nameSub.length-1] + "&a' stopped!");
    }

    /**
     * Действия при запуске
     */
    public abstract void onStartup();

    /**
     * Действия при остановке
     */
    public abstract void onStopping();

    /**
     * Проверка на то, запущен ли Scheduler
     * @return True если запущен
     */
    public boolean isStarted() {
        return started;
    }

    public DarkWaterJavaPlugin getPlugin() {
        return this.plugin;
    }

    public String getName() {
        var nameSub = this.name.split("\\.");
        return nameSub[nameSub.length-1];
    }
}
