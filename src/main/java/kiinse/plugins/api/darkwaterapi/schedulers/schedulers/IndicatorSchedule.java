package kiinse.plugins.api.darkwaterapi.schedulers.schedulers;

import kiinse.plugins.api.darkwaterapi.DarkWaterAPI;
import kiinse.plugins.api.darkwaterapi.files.config.utils.Config;
import kiinse.plugins.api.darkwaterapi.indicators.interfaces.IndicatorManager;
import kiinse.plugins.api.darkwaterapi.loader.DarkWaterJavaPlugin;
import kiinse.plugins.api.darkwaterapi.schedulers.interfaces.Scheduler;
import kiinse.plugins.api.darkwaterapi.utilities.PlayerUtils;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;

import static org.bukkit.Bukkit.getServer;

public class IndicatorSchedule extends Scheduler {
    private final IndicatorManager indicators = DarkWaterAPI.getInstance().getIndicatorManager();
    private int schedulerID;

    public IndicatorSchedule(DarkWaterJavaPlugin plugin) {
        super(plugin, IndicatorSchedule.class.getName());
    }

    @Override
    public void onStartup() {
        if (plugin.getConfiguration().getBoolean(Config.ACTIONBAR_INDICATORS) && Bukkit.getServer().getPluginManager().getPlugin("PlaceholderAPI") != null) {
            schedulerID = getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
                for (var player : Bukkit.getOnlinePlayers()) {
                    if (PlayerUtils.isSurvivalAdventure(player)) {
                        PlayerUtils.sendActionBar(player, PlaceholderAPI.setPlaceholders(player, indicators.getIndicators()));
                    }
                }
            }, 0L, 20L);
        }
    }

    @Override
    public void onStopping() {
        plugin.getServer().getScheduler().cancelTask(schedulerID);
    }

}
