package kiinse.plugins.api.darkwaterapi.schedulers;

import kiinse.plugins.api.darkwaterapi.loader.DarkWaterJavaPlugin;
import kiinse.plugins.api.darkwaterapi.schedulers.interfaces.Scheduler;
import kiinse.plugins.api.darkwaterapi.schedulers.interfaces.SchedulersManager;
import kiinse.plugins.api.darkwaterapi.utilities.DarkWaterUtils;

public class SchedulersManagerImpl extends SchedulersManager {

    public SchedulersManagerImpl(DarkWaterJavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public void registerSchedule(Scheduler scheduler) throws IllegalArgumentException {
        if (scheduler == null) {
            throw new IllegalArgumentException("Scheduler is null!");
        }
        if (scheduler.getName() == null) {
            throw new IllegalArgumentException("Scheduler name is null!");
        }
        if (DarkWaterUtils.isStringEmpty(scheduler.getName())) {
            throw new IllegalArgumentException("Scheduler name is empty!");
        }
        if (scheduler.getPlugin() == null) {
            throw new IllegalArgumentException("Scheduler plugin is null!");
        }
        if (hasScheduler(scheduler)) {
            throw new IllegalArgumentException("Scheduler with same name '" + scheduler.getName() + "' already exist!");
        }
        register(scheduler);
    }
}
