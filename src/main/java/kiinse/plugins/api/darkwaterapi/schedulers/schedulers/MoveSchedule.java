package kiinse.plugins.api.darkwaterapi.schedulers.schedulers;

import kiinse.plugins.api.darkwaterapi.loader.DarkWaterJavaPlugin;
import kiinse.plugins.api.darkwaterapi.schedulers.interfaces.Scheduler;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import static org.bukkit.Bukkit.getServer;

public class MoveSchedule extends Scheduler {

    private final Map<UUID, Integer> notMovingMap = new HashMap<>();

    private int schedulerMoveID;

    public MoveSchedule(DarkWaterJavaPlugin plugin) {
        super(plugin, MoveSchedule.class.getName());
    }

    @Override
    public void onStartup() {
        schedulerMoveID = getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> movingAddValues(notMovingMap.entrySet().iterator()), 1, 20);
    }

    @Override
    public void onStopping() {
        plugin.getServer().getScheduler().cancelTask(schedulerMoveID);
    }


    private void movingAddValues(Iterator<Map.Entry<UUID, Integer>> iterator) {
        while (iterator.hasNext()) {
            var entry = iterator.next();
            entry.setValue(entry.getValue() + 1);
        }
    }

    public Map<UUID, Integer> getNotMovingMap() {
        return notMovingMap;
    }
}
