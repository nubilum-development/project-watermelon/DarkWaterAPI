package kiinse.plugins.api.darkwaterapi.schedulers.interfaces;

import kiinse.plugins.api.darkwaterapi.loader.DarkWaterJavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SuppressWarnings("unused")
public abstract class SchedulersManager {

    private final List<Scheduler> schedulers = new ArrayList<>();
    private final DarkWaterJavaPlugin plugin;

    protected SchedulersManager(DarkWaterJavaPlugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Регистрация scheduler
     * @param scheduler {@link Scheduler}
     */
    public abstract void registerSchedule(Scheduler scheduler);

    /**
     * Запуск scheduler
     * @param scheduler {@link Scheduler}
     * @throws Exception Возникает если scheduler уже запущен
     */
    public void startScheduler(Scheduler scheduler) throws Exception {
        for (var schedule : schedulers) {
            if (Objects.equals(schedule.getName(), scheduler.getName())) {
                if (!schedule.isStarted()) {
                    schedule.start();
                } else {
                    throw new Exception("This scheduler '" + scheduler.getName() + "' already started!");
                }
            }
        }
    }

    /**
     * Остановка scheduler
     * @param scheduler {@link Scheduler}
     * @throws Exception Возникает если scheduler уже остановлен
     */
    public void stopScheduler(Scheduler scheduler) throws Exception {
        for (var schedule : schedulers) {
            if (Objects.equals(schedule.getName(), scheduler.getName())) {
                if (schedule.isStarted()) {
                    schedule.stop();
                } else {
                    throw new Exception("This scheduler '" + scheduler.getName() + "' already stopped!");
                }
            }
        }
    }

    /**
     * Остановка всех зарегистрированных scheduler
     */
    public void stopSchedules() {
        for (var scheduler : schedulers) {
            scheduler.stop();
        }
    }

    public boolean hasScheduler(Scheduler scheduler) {
        for (var schedule : schedulers) {
            if (Objects.equals(schedule.getName(), scheduler.getName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Получения зарегистрированного scheduler по имени
     * @param name Имя scheduler
     * @return Зарегистрированный scheduler
     */
    public Scheduler getSchedulerByName(String name) {
        for (var scheduler : schedulers) {
            if (Objects.equals(scheduler.getName(), name)) {
                return scheduler;
            }
        }
        return null;
    }

    /**
     * Удаление scheduler из регистрации
     * @param scheduler {@link Scheduler}
     * @throws Exception Возникает если scheduler не был найден в зарегистрированных
     */
    public void unregister(Scheduler scheduler) throws Exception {
        if (!hasScheduler(scheduler)) {
            throw new Exception("This scheduler '" + scheduler.getName() + "' not found!");
        }
        var iterator = schedulers.listIterator();
        while (iterator.hasNext()) {
            var schedule = iterator.next();
            if (Objects.equals(schedule.getName(), scheduler.getName())) {
                if (schedule.isStarted()) {
                    schedule.stop();
                }
                iterator.remove();
            }
        }
        plugin.sendLog("Scheduler '&b" + scheduler.getName() + "&a' by plugin '&b" + scheduler.getPlugin().getName() + "&a' has been unregistered!");
    }

    protected void register(Scheduler scheduler) {
        schedulers.add(scheduler);
        plugin.sendLog("Scheduler '&b" + scheduler.getName() + "&a' by plugin '&b" + scheduler.getPlugin().getName() + "&a' has been registered!");
        scheduler.start();
    }

    /**
     * Получение списка всех зарегистрированных scheduler
     * @return List {@link Scheduler}
     */
    public List<Scheduler> getAllSchedulers() {
        return new ArrayList<>(schedulers);
    }
}
