package kiinse.plugins.api.darkwaterapi.schedulers.schedulers;

import kiinse.plugins.api.darkwaterapi.loader.DarkWaterJavaPlugin;
import kiinse.plugins.api.darkwaterapi.schedulers.interfaces.Scheduler;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import static org.bukkit.Bukkit.getServer;

public class JumpSchedule extends Scheduler {

    private final Map<UUID, Integer> jumpingMap = new HashMap<>();

    private int schedulerJumpID;

    public JumpSchedule(DarkWaterJavaPlugin plugin) {
        super(plugin, JumpSchedule.class.getName());
    }

    @Override
    public void onStartup() {
        schedulerJumpID = getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> jumpingAddValues(jumpingMap.entrySet().iterator()), 1, 20);
    }

    @Override
    public void onStopping() {
        plugin.getServer().getScheduler().cancelTask(schedulerJumpID);
    }

    private void jumpingAddValues(Iterator<Map.Entry<UUID, Integer>> iterator) {
        while (iterator.hasNext()) {
            var entry = iterator.next();
            entry.setValue(entry.getValue() + 1);
            if (entry.getValue() >= 2) {
                iterator.remove();
            }
        }
    }

    public Map<UUID, Integer> getJumpingMap() {
        return jumpingMap;
    }
}
