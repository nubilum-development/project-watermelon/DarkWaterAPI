package kiinse.plugins.api.darkwaterapi.files.messages.utils;

import kiinse.plugins.api.darkwaterapi.files.messages.interfaces.ReplaceKeys;

public enum Replace implements ReplaceKeys {
    LOCALE,
    LOCALES,
    PLAYER,
    PAGE,
    CODE,
    STATISTIC,
    PLUGIN
}
