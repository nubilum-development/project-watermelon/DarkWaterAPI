package kiinse.plugins.api.darkwaterapi.files.messages.utils;

import kiinse.plugins.api.darkwaterapi.files.messages.interfaces.MessagesKeys;

public enum Message implements MessagesKeys {
    LOCALES_GUI,
    EXIT,
    SET_THIS_LOCALE,
    NO_PERMISSION,
    INSUFFICIENT_PARAMETER,
    REDUNDANT_PARAMETER,
    COMMAND_NOT_FOUND,
    REFLECTION_ERROR,
    NOT_PLAYER,
    STATUS_COMMAND,
    LOCALE_CHANGED,
    LOCALE_NOT_FOUND,
    INFO_COMMAND,
    GET_COMMAND,
    PLAYER_NOT_FOUND,
    FIRST_JOIN,
    LOCALES_LIST,
    PREFIX,
    RESOURCE_DECLINED,
    PLUGIN_NOT_FOUND,
    PLUGIN_RELOADED,
    PLUGIN_DISABLED,
    PLUGIN_ENABLED,
    PLUGIN_ERROR,
    GUI_CURRENT_PAGE,
    GUI_NEXT_PAGE,
    GUI_PREVIOUS_PAGE,
    GENERATED_CODE,
    STATISTIC
}
