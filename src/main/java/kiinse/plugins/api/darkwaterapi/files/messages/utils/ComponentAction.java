package kiinse.plugins.api.darkwaterapi.files.messages.utils;

public enum ComponentAction {
    URL,
    CMD,
    COPY,
    HOVER
}
