package kiinse.plugins.api.darkwaterapi.files.config.utils;

import kiinse.plugins.api.darkwaterapi.files.config.interfaces.ConfigKeys;

public enum Config implements ConfigKeys {
    FIRST_JOIN_MESSAGE,
    ACTIONBAR_INDICATORS,
    LOCALE_DEFAULT,
    PG_HOST,
    PG_PORT,
    PG_LOGIN,
    PG_PASSWORD,
    PG_DBNAME,
    DEBUG,
    REST_ENABLE,
    REST_PORT,
    REST_NAME,
    REST_AUTH_ENABLE,
    REST_AUTH_TYPE,
    REST_BEARER_USERS,
    REST_BEARER_SECRET,
    REST_BEARER_EXPIRE,
    REST_BASIC_LOGIN,
    REST_BASIC_PASSWORD,
    REST_ENCRYPTED_DATA,
    REST_SERVICE_COMMANDS,
    REST_SERVICE_CODE,
    CONFIG_VERSION
}
