package kiinse.plugins.api.darkwaterapi.files.filemanager.utils;

import kiinse.plugins.api.darkwaterapi.files.filemanager.interfaces.FilesKeys;

public enum File implements FilesKeys {

    DATA_JSON,
    CONFIG_YML,
    CONFIG_TMP_YML,
    CONFIG_OLD_YML,
    STATISTIC_JSON,
    LOCALES_YML
}
