package kiinse.plugins.api.darkwaterapi.files.messages;

import kiinse.plugins.api.darkwaterapi.files.messages.interfaces.ComponentLabels;
import kiinse.plugins.api.darkwaterapi.files.messages.utils.ComponentAction;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.event.HoverEvent;
import org.bukkit.ChatColor;

public class ParseComponentLabels implements ComponentLabels {

    @Override
    public Component parseMessage(String msg) {
        var result = Component.text(msg);
        if (hasTextEndLabels(msg)) {
            result = Component.text("");
            var split = msg.split("<");
            for (var arg : split) {
                var hasTextLabels = hasTextLabels(arg);
                var hasTextEndLabels = hasTextEndLabels(arg);
                if (!hasTextLabels && !hasTextEndLabels && !arg.isEmpty()) {
                    result = result.append(Component.text(arg));
                } else {
                    var text = arg.split(">");
                    if (hasTextLabels) {
                        var label = text[0].split("=");
                        result = result.append(getComponent(text[1], label[1], ComponentAction.valueOf(label[0])));
                    }
                    if (hasTextEndLabels && text.length > 1) {
                        result = result.append(Component.text(text[1]));
                    }
                }
            }
        }
        return result;
    }

    private boolean hasTextLabels(String msg) {
        for (var action : ComponentAction.values()) {
            if (msg.contains(action + "=")) {
                return true;
            }
        }
        return false;
    }

    private boolean hasTextEndLabels(String msg) {
        for (var action : ComponentAction.values()) {
            if (msg.contains("/" + action + ">")) {
                return true;
            }
        }
        return false;
    }

    private Component getComponent(String text, String click, ComponentAction action) {
        var message = click.split("::");
        return switch (action) {
            case CMD -> Component.text(text).clickEvent(ClickEvent.runCommand(message[0])).hoverEvent(HoverEvent.showText(getHoverText(message)));
            case URL -> Component.text(text).clickEvent(ClickEvent.openUrl(message[0])).hoverEvent(HoverEvent.showText(getHoverText(message)));
            case COPY -> Component.text(text).clickEvent(ClickEvent.copyToClipboard(message[0])).hoverEvent(HoverEvent.showText(getHoverText(message)));
            case HOVER -> Component.text(text).hoverEvent(HoverEvent.showText(getHoverText(message)));
        };
    }

    private Component getHoverText(String[] text) {
        return Component.text(ChatColor.translateAlternateColorCodes('&', text.length > 1 ? text[1] : text[0]));
    }

}