package kiinse.plugins.api.darkwaterapi.files.filemanager;

import kiinse.plugins.api.darkwaterapi.files.filemanager.interfaces.FilesKeys;
import kiinse.plugins.api.darkwaterapi.files.filemanager.interfaces.FilesManager;
import kiinse.plugins.api.darkwaterapi.loader.DarkWaterJavaPlugin;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Класс работы с JSON файлами
 */
@SuppressWarnings("unused")
public class JsonFile extends FilesManager {

    private final DarkWaterJavaPlugin plugin;
    private final File file;

    /**
     * Создание файла при его отсутствии
     * @param plugin Плагин
     * @param fileName Файл
     */
    public JsonFile(DarkWaterJavaPlugin plugin, FilesKeys fileName) {
        super(plugin);
        this.plugin = plugin;
        if (isFileNotExists(fileName)) {
            copyFile(fileName);
        }
        this.file = getFile(fileName);
    }

    /**
     * Чтение json внутри файла
     * @return JSONObject
     * @throws IOException При ошибках во время копирования
     */
    public JSONObject getJsonFromFile() throws IOException {
        try (var br = new BufferedReader(new FileReader(file.getAbsolutePath()))) {
            var line = br.readLine();
            if (line == null) {
                return new JSONObject();
            }
            var json = new JSONObject(Files.readString(Paths.get(file.getAbsolutePath())));
            plugin.sendLog("File '&b" + file.getName() + "&a' loaded");
            return json;
        }
    }

    /**
     * Запись JSONObject в файл
     * @param json JSONObject
     * @throws IOException При ошибках во время записи
     */
    public void saveJsonToFile(JSONObject json) throws IOException {
        if (!file.exists() && file.createNewFile()) {
            plugin.sendLog("File '&b" + file.getName() + "&a' created");
        }
        var lines = List.of(json.toString());
        Files.write(Paths.get(file.getAbsolutePath()), lines, StandardCharsets.UTF_8);
        plugin.sendLog("File '&b" + file.getName() + "&a' saved");
    }

}
