package kiinse.plugins.api.darkwaterapi.files.messages.interfaces;

import net.kyori.adventure.text.Component;

public interface ComponentLabels {

    Component parseMessage(String msg);
}
