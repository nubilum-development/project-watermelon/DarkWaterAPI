package kiinse.plugins.api.darkwaterapi.rest.actions;

import kiinse.plugins.api.darkwaterapi.DarkWaterAPI;
import kiinse.plugins.api.darkwaterapi.files.config.utils.Config;
import kiinse.plugins.api.darkwaterapi.files.filemanager.YamlFile;
import kiinse.plugins.api.darkwaterapi.rest.utils.ExecuteCMD;
import kiinse.plugins.api.darkwaterapi.rest.utils.RestStatus;
import kiinse.plugins.api.darkwaterapi.rest.utils.RestUtils;
import kiinse.plugins.api.darkwaterapi.utilities.cryptography.interfaces.RSADarkWater;
import org.bukkit.Bukkit;
import org.json.simple.JSONObject;
import services.moleculer.context.Context;
import services.moleculer.service.Action;

import java.util.logging.Level;

@SuppressWarnings("unchecked")
public class CommandsAction implements Action {

    private final DarkWaterAPI darkWaterAPI;
    private final RSADarkWater rsa;
    private final YamlFile config;
    private final ExecuteCMD executor;

    public CommandsAction(DarkWaterAPI darkWaterAPI, RSADarkWater rsa) {
        this.darkWaterAPI = darkWaterAPI;
        this.rsa = rsa;
        this.executor = new ExecuteCMD(darkWaterAPI.getServer());
        this.config = darkWaterAPI.getConfiguration();
    }

    @Override
    public Object handler(Context context) throws Exception {

        if (!config.getBoolean(Config.REST_SERVICE_CODE)) {
            return RestUtils.createAnswer(RestStatus.ERROR_SERVICE_DISABLED);
        }

        if (!config.getBoolean(Config.REST_AUTH_ENABLE)) {
            return RestUtils.createAnswer(RestStatus.ERROR_AUTHENTICATION_DISABLED);
        }

        var cmd = context.params.get("cmd", "");
        if (cmd.isBlank()) {
            var json = new JSONObject();
            json.put("publicKey", new JSONObject(rsa.getPublicKeyJson().toMap()));
            return RestUtils.createAnswer(RestStatus.SUCCESS, json);
        }

        try {
            var cmdFinal = config.getBoolean(Config.REST_ENCRYPTED_DATA) ? rsa.decryptMessage(cmd) : cmd;
            var isSuccess = Bukkit.getScheduler().callSyncMethod(darkWaterAPI, () -> Bukkit.dispatchCommand(executor, cmdFinal)).get();
            return RestUtils.createAnswer(RestStatus.SUCCESS, String.valueOf(isSuccess));
        } catch (InterruptedException e) {
            darkWaterAPI.sendLog(Level.SEVERE, "Error on execute command from Rest! Message:\n" + e.getMessage());
            Thread.currentThread().interrupt();
            return RestUtils.createAnswer(RestStatus.ERROR, e);
        }
    }
}
