package kiinse.plugins.api.darkwaterapi.rest.utils;

public enum AuthTypes {
    BEARER,
    BASIC
}
